# Ancestry deconvolution and partial polygenic score can improve susceptibility predictions in recently admixed individuals

___

This repository contains scripts and methods used in the published project: *"Ancestry deconvolution and partial polygenic score can improve susceptibility predictions in recently admixed individuals"*. You can find the paper at https://www.nature.com/articles/s41467-020-15464-w .
## Contents
- partialPolyScorer - an improved version of the software used to compute ancestry specific partial polygenic scores. The directory contains a python script and some example files.
- simulation.R - the script used to perform the simulation described in section "Proposed model and workflow".

## partialPolyScorer

This tool can be used to compute polygenic scores and ancestry specific partial polygenic scores from:
- a VCF file. It should be phased and its genotype fields should be annotated with a "LA" subfield which states the local ancestry of each allele, encoded as 0-based, increasing integer. E.g. if the FORMAT field contains "GT:LA" each genotype field should contain "0|0:1|0" where 0|0 is the genotype and 1,0 is the ancestry of the first,second allele. genotypes are accepted and to indicate unknown ancestry you should use "." (e.g 1|. or .|.).
- a file with the summary statistics for trait-associated snps
- a file with the list of individuals to be considered as reference for the standardization


With -A you can supply the ancestries for which you want to compute the partial polygenic score, they should match the ancestries in the vcf file. If not provided only the total PS will be computed.

The tool should be used as following:

```sh
partialPolyScorer -A ANC1,ANC2,... VCF_FILE SUMMARY_STATISTICS_FILE:CHR_FIELD:POS_FIELD:EFFECT_ALLELE_FIELD:BETA_FIELD REFERENCE_SAMPLES_LIST
```

e.g., with example files:

```sh
partialPolyScorer -A 0,1 example.vcf.gz example.summstats.gz:1:2:3:4 example_ref.list > output
```

The output has the following fields:

| field            | description |
|------------------|----------------------------------------------------------------------------------|
| ID               | ID of the individual                                                             |
| genomic\_set\_ID | ID of the local ancestry (always matching with ID except for --print\_all option)|
| genomic\_set     | name of the local ancestry                                                       |
| standardized\_PS | standardized PS                                                                  |
| raw\_PS          | raw PS                                                                           |
| snpcount         | number of snps used                                                              |
| refmean          | mean of the reference group to standardize PS                                    |
| refstd           | dev. st of the reference group to standardize PS                                 |

See more information and the full option list with `partialPolyScorer -h`
