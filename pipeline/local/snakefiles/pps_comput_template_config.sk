# necessary variables
CHRS=list(range(1,23))
pop_id="" # the path to the metafile, containing 2 tab-delimited fields: population,individualID
summstat_input="" #path of the summary statistics files, should contain the wildcard {trait}, eg: my/summary/stats/path/{trait}.snps.gz
vcf_input="" #a vcf.gz with all individuals of interest and their references, should contain the wildcard {chr}, eg: my/vcfs/chr{chr}.vcf.gz
laf_input="" #the local ancestry file containing ancestry information, should contain the wildcard {chr}, eg:

populations={'ALL':['EUR','EAS','AFR'],'EURAFRrefEUR':['EURAFR','EUR'],'EUREASrefEAS':['EUREAS','EAS']} #a dictionary of batches/populations/superpopulations to deal with different sample sets
standardization_pop={'heightEUR':'EUR','heightEAS':'EAS'} #a dictionary of trait-sampleset used to define the populations on which standardizing each trait
clumping_pop=standardization_pop #as above to perform the clumping, can also be different
prsice_pop=standardization_pop #as above to perform prsice analysis, can also be different
pheno={'heightEUR':'height','heightEAS':'height'} #dictionary between trait name and real phenotipe associated, used in prsice
all_traits=['heightEUR','heightEAS'] #a list that contains all traits to analyze, each trait corresponds to a summary statistics file (so different summary statistics can be present for the same phenotype but we encode them as different traits)
aspPS_ancs="0,1,2" #the ancestries for which computing partial ps
maf_cutoff=0.01 #cutoff on the summary statistics file
clumpp1=0.5 #the snp significance cutoff for clumping: this will keep all independent snps with pvalue<clumpp1
clumpr2=0.05 #the liinkage disequilibrium cutoff for clumping
prsice_pheno_file="" # a tab delimited, headed file with a column for each phenotype of interest and covariate for the individuals contained in the vcf dataset 
prsice_covariates="age,age2,age3,sex,gen_batch,PC1,PC2,PC3,PC4,PC5,PC6" #covariates to use in prsice
prsice_cov_factors= "sex,gen_batch" #those covariates that are factors(non-continuous)
binary_trait="F" #T if the trait is binary, F otherwise


####### custom rules #######
############################

# separates pcs, adds age^2 age^3
rule covariate_table:
	input: '{dataset}.list','/ebc_data/marnetto/prj/phenotypes/dataset/UKBB/admixedUKBB.ukb29612.gz'
	output: 'summstat/prsice/{dataset}.phenotypes.txt'
	params: pcsfield=2,agefield=3
	shell:'''\
zcat {input[1]}\
| awk -v agef={params.agefield} -v pcf={params.pcsfield} \
	'NR==1	{{OFS="\\t"; ORS=""; print $0; for (i=1;i<=40;i++) print "\\tPC"i; print "\\t"$(agef)"2",$(agef)"3\\n"; ORS="\\n"; next}}\
		{{pcs=$(pcf); gsub(";","\\t",pcs); print $0,pcs,$(agef)^2,$(agef)^3}}' \
| sort -k1,1 |\
join -t $'\\t'\
	<((echo 'id'; cat {input[0]} )| sort) \
	-\
| sort -k1,1r > {output}
'''

ukb_dir=''
ukb_pheno={
        'height':'50_raw.gwas.imputed_v3.both_sexes.tsv.bgz',
        'BMI':'21001_raw.gwas.imputed_v3.both_sexes.tsv.bgz'}

#ruleorder: asnp_raw_exception > raw_asnp
#modifies the summary statistics raw input , if needed uncomment ruleorder to give priority to this rule
#might be necessary to split a file with multiple summary stats in individual files
rule asnp_raw_exception:
	input: lambda w: ukb_dir+ukb_pheno[w.trait]
	output: '{trait,height|BMI}/wg.snps.gz' 
	resources: mem=512
	shell:'''
zcat {input} | sed 's/:/\\t/g' | awk -v OFS='\\t' 'NR!=1 {{print $1,$2,$1"_"$2,$4,$3,$6,$11,$12,$14}}' | sort -k1,1 -k2,2n -k9,9g | gzip > {output}'''
