#!/usr/bin/env python
from argparse import ArgumentParser
from sys import stderr,stdout,stdin,exit,exc_info
from math import sqrt
import numpy as np
import gzip
import re
from threadpoolctl import threadpool_limits


import time
from datetime import timedelta
np.seterr('raise')
t0=time.time()

class PStableold: #old PStable
	def __init__(self,indlist,ancs,refindidx=None):
		self.indlist=indlist
		self.N=len(indlist)*2
		self.ancs=ancs
		self.S=len(ancs)
		self.m=0
		if self.S!=0:
			self.PS=np.zeros(shape=(self.N,1+self.S*self.N),dtype=float)
			self.count=np.zeros(shape=(1,1+self.S*self.N),dtype=int)
		else:
			self.addsnp=self.addsnp_onlytotal
			self.PS=np.zeros(shape=(self.N,1),dtype=float)
			self.count=np.zeros(shape=(1,1),dtype=int)
		if refindidx==None:
			refindidx=list(range(N/2))
		refhapidx=[idx*2+hap for idx in refindidx for hap in [0,1]]
		self.refindidx=refindidx
		self.refhapidx=refhapidx

	def addsnp(self,ea,B,vcfline,P):
		try:
			haps,f,split_gts=parsevcfline(vcfline,ea)
		except ValueError:
			return(1)
		laidx=f.index("LA")
		LAs = [myidx(self.ancs,y) for x in split_gts for y in x[laidx].split("|")] #extract local ancestry status from GT fields
		#now create a mask that has 1 where I add the current snp and 0 where not, with always 1 in the first cell (the total PS) and 1 where the local ancestry says so
		mask=np.zeros(shape=self.count.shape)
		for i in [0]+[1+idx*self.S+anc for idx,anc in enumerate(LAs) if anc!=None]: #retrieve indexes of ancestries that have this snp (always the first column, the total PS)
			mask[0,i]=1
		self.count=self.count+mask
		with threadpool_limits(limits=P, user_api='blas'):
			toadd=np.dot(haps,mask)*B
			self.PS = self.PS + toadd #alleles * B_score * mask are summed to the previous PS sum
		self.m+=1
		return(0)

	def addsnp_onlytotal(self,ea,B,vcfline,P):
		try:
			haps,f,split_gts=parsevcfline(vcfline,ea)
		except ValueError:
			return(1)
		self.m+=1
		self.PS = self.PS + haps*B
		return(0)
		
	def printout(self,printall,minsnps,p):
		print('#ID\tgenomic_set_ID\tgenomic_set\tstandardized_PS\traw_PS\tsnpcount\trefmean\trefstd')
		if self.S==0:
			self.count[0,0]=self.m
		for i in range(0,self.N,2):
			ind_id=self.indlist[int(i/2)]
			if printall:
				pPS_rangea=[0]+[y*self.S+x+1 for y in range(0,self.N,2) for x in range(self.S)]
				pPS_rangeb=[0]+[(y+1)*self.S+x+1 for y in range(0,self.N,2) for x in range(self.S)]
			else:
				pPS_rangea=[0]+[i*self.S+x+1 for x in range(self.S)]
				pPS_rangeb=[0]+[(i+1)*self.S+x+1 for x in range(self.S)]
			for a,b in zip(pPS_rangea,pPS_rangeb):
				if self.count[0,a]+self.count[0,b]>=minsnps*2:
					set_id="total" if a==0 else self.indlist[int((a-1)/(2*self.S))]
					set_type="total" if a==0 else self.ancs[(a-1)%self.S]
					na=self.count[0,a]
					nb=self.count[0,b]
					refPSa=np.array([self.PS[x,a] for x in self.refhapidx[::2]])
					refPSb=np.array([self.PS[x,b] for x in self.refhapidx[1::2]])
					try:
						result=standardizePS(self.PS[i,a],self.PS[i+1,b],refPSa,refPSb,na,nb,self.m,p)
					except TypeError:
						continue
					print("\t".join([ind_id,set_id,set_type]+[str.format('{:g}',x) for x in result]))


class PStable:
	def __init__(self,indlist,ancs,M,refindidx=None):
		self.indlist=indlist
		self.N=len(indlist)*2
		self.ancs=ancs
		self.S=len(ancs)
		self.M=M
		self.m=0
		self.B=[]
		self.GT=np.zeros(shape=(self.M,self.N),dtype=int)
		self.LA=np.zeros(shape=(self.M,self.N),dtype=int)
		if refindidx==None:
			refindidx=list(range(N/2))
		refhapidx=[idx*2+hap for idx in refindidx for hap in [0,1]]
		self.refindidx=refindidx
		self.refhapidx=refhapidx

	def addsnp(self,ea,B,vcfline,P):
		try:
			haps,f,split_gts=parsevcfline(vcfline,ea)
		except ValueError:
			return(1)
		self.GT[self.m]=haps.T
		laidx=f.index("LA")
		LAs = [onebasedidx(self.ancs,y) for x in split_gts for y in x[laidx].split("|")] #extract local ancestry status from GT fields
		self.LA[self.m]=LAs
		self.m+=1
		self.B.append(B)
		return(0)

	def printout(self,printall,minsnps,p):
		self.GT=self.GT[:self.m]
		self.LA=self.LA[:self.m]
		self.ref=self.GT[:,self.refhapidx]
		#print("\n".join(str(x) for x in self.LA))
		print('#ID\tgenomic_set_ID\tgenomic_set\tstandardized_PS\traw_PS\tsnpcount\trefmean\trefstd')
		for set_idx,set_type in enumerate(["total"]+self.ancs):
			#print([set_idx,set_type])
			if set_type=="total":
				self.PS=np.dot(self.B,self.GT)
				self.count=np.repeat(self.m,self.N)
			else:
				asLA= self.LA == set_idx
				asGT=self.GT*asLA
				self.PS=np.dot(self.B,asGT)
				self.count=np.sum(asLA,0)
			#print(";".join([str(x) for x in self.count]))
			for i,ind_id in enumerate(self.indlist):
				if set_type=="total":
					refPSa=np.dot(self.B,self.ref[:,::2])
					refPSb=np.dot(self.B,self.ref[:,1::2])
				elif self.count[i*2]+self.count[i*2+1]<minsnps*2:
					continue
				else:
					refPSa=np.dot(self.B,self.ref[:,::2]*np.array(asLA[:,i*2],ndmin=2).T)
					refPSb=np.dot(self.B,self.ref[:,1::2]*np.array(asLA[:,i*2+1],ndmin=2).T)
				set_id=ind_id
				na=self.count[i*2]
				nb=self.count[i*2+1]
				try:
					result=standardizePS(self.PS[i*2],self.PS[i*2+1],refPSa,refPSb,na,nb,self.m,p)
				except TypeError:
					continue

				print("\t".join([ind_id,set_id,set_type]+[str.format('{:g}',x) for x in result]))


def standardizePS(rawPSa,rawPSb,refPSa,refPSb,na,nb,m,p):
	n=na+nb
	raw_PS=(rawPSa+rawPSb)/n #raw_PS is sum of PSa and PSb divided by sum of their snpcount
	refPS=(refPSa+refPSb)/n #the same happens for reference PS... vectorial sum of PSas and PSbs divided by sum of their snpcount
	refmean=np.mean(refPS)
	refstd=np.std(refPS,ddof=1)
	c=sqrt(n/(m*2)) if p else 1
	try:
		std_PS=(raw_PS-refmean)/(c*refstd)
	except FloatingPointError:
		#print(";".join([str(x) for x in [rawPSa,rawPSb,"refPSa","refPSb",na,nb,m,p]]),file=stderr,flush=True)
		return(None)
	return((std_PS,raw_PS,n/2,refmean,refstd))

def parsevcfline(vcfline,ea):
	chrom,pos,rsid,ref,alt,qual,filt,info,form,*gts=vcfline.rstrip('\r\n').split('\t') #parse vcf line
	f=form.split(":") #parse FORMAT field
	split_gts=[x.split(":") for x in gts]
	gtidx=f.index("GT")
	haps=[int_or_none(y) for x in split_gts for y in re.split("[|/]",x[gtidx])] #extract haploid alleles from GT fields
	haps=np.array(haps,ndmin=2).T
	if ea != alt:
		if ea == ref:
			haps=abs(haps-1) #polarize alleles so that effect allele = 1
		else:
			raise ValueError('effect allele '+ea+' absent at snp '+rsid)
	return [haps,f,split_gts]

def int_or_none(x):
	try:
		return(int(x))
	except ValueError:
		return(None)

def myidx(l,v):
	try:
		return(l.index(str(v)))
	except ValueError:
		return(None)

def onebasedidx(l,v):
	try:
		return(1+l.index(str(v)))
	except ValueError:
		return(0)

def safe_open(filename):
	if re.search('\.gz$', filename):
		f=gzip.open(filename,'rt')
	else:
		f=stdin if filename=="-" else open(filename,'r')
	return(f)

def timethis():
	global t0
	t=time.time()
	timeout=str(timedelta(seconds=t-t0))
	t0=t
	return(timeout)

def main():
	usage = '''partialPolyScorer -A ANC1,ANC2,... VCF_FILE SUMMARY_STATISTICS_FILE:CHR_FIELD:POS_FIELD:EFFECT_ALLELE_FIELD:BETA_FIELD REFERENCE_SAMPLES_LIST
This tool can be used to compute polygenic scores and ancestry specific partial polygenic scores from:
- a VCF file
- a file with the summary statistics for trait-associated snps
- a file with the list of individuals to be considered as reference for the standardization
The vcf file should be phased and its genotype fields should be annotated with a "LA" subfield which states the local ancestry of each allele, encoded as 0-based, increasing integer. E.g. if the FORMAT field contains "GT:LA" each genotype field should contain "0|0:1|0" where 0|0 is the genotype and 1,0 is the ancestry of the first,second allele. genotypes are accepted and to indicate unknown ancestry you should use "." (e.g 1|. or .|.).
With -A you should supply the ancestries for which you want to compute the partial polygenic score, they should match the ancestries in the vcf file. If is not provided only the total PS will be computed.
The output has the following fields:
	ID		ID of the individual
	genomic_set_ID	ID of the local ancestry (always matching with ID except for --print_all option)
	genomic_set	name of the local ancestry
	standardized_PS	standardized PS
	raw_PS		raw PS
	snpcount	number of snps used
	refmean		mean of the reference group to standardize PS
	refstd		dev. st of the reference group to standardize PS
	'''
	parser = ArgumentParser(usage=usage)

	parser.add_argument('-a','--print_all', dest='print_all', default=False, help='print all combinations of individual and local ancestry pattern, including those where individual and pattern don\'t match. For testing purposes',action='store_true')
	parser.add_argument('-o', dest='old', default=False, help='use old algorithm. Muuuch longer but allows option -a',action='store_true')

	parser.add_argument('-A', '--Ancs', dest='anc', default=None, type=str, help='comma separated list of ancestries present in the vcf, to compute partial PS on.. If absent only computes traditional total PS')
	parser.add_argument('-m', '--min_snps', dest='minsnps', default=10, type=int, help='minimum number of snps needed to compute a partial PS, defaults to 10')
	parser.add_argument('-P', dest='threads', default=1, type=int, help='number of threads to allow in parallel for array operations in numpy')
	parser.add_argument('-p','--sqrtp_corr', dest='p', default=False, help='correct the standardized pPS for 1/sqrt(p) where p is the genomic fraction',action='store_true')
	parser.add_argument('vf',metavar='VCF_FILE',help='a VCF file, possibly with local ancestry annotated as LA subfield in the genotype field (eg. FORMAT field: \'GT:LA\')')
	parser.add_argument('ssf', metavar='SUMMARY_STATISTICS_FILE:CHR_FIELD:POS_FIELD:EFFECT_ALLELE_FIELD:BETA_FIELD', help='a file with the summary statistics for trait-associated snps followed by the field numbers where to find chromosome, position, effect allele, beta, all colon-separated')
	parser.add_argument('rsf',metavar='REFERENCE_SAMPLES_FILE', help='a file with the list of individuals to be considered as reference for the standardization')

	args = parser.parse_args()
	if args.print_all and not args.old:
		raise RuntimeError('print_all option only available with old algorithm (option -o)')
	if args.anc!=None:
		ancs=args.anc.split(",")
	else:
		ancs=[]
	#open vcf and extract header, then stop
	vcffile=safe_open(args.vf)
	for line in vcffile:
		if re.match(r'^#CHROM', line):
			header= line.rstrip('\r\n').split('\t')
			break

	if len(header) < 10 or header[0]!="#CHROM" or header[8]!="FORMAT":
		raise IOError(vcf_infile+': Unexpected file format (weird header)')

	#index haplotypes in header and defines N
	indlist=header[9:]
	print(str(len(indlist))+" diploid samples ["+timethis()+"]",file=stderr,flush=True)

	#define reference individuals list and index individuals and haplotypes
	reflistfile=safe_open(args.rsf)
	reflist=[x.rstrip() for x in  reflistfile.readlines()]
	reflistfile.close()
	try:
		refindidx=[indlist.index(x) for x in reflist]
	except ValueError as e:
		raise RuntimeError("the following individual, requested as reference, is not present in vcf:"+e.args[0][:-15]) from None
	print(str(len(refindidx))+" diploid reference samples ["+timethis()+"]",file=stderr,flush=True)

	#load summary statistics file into a dictionary of tuples
	ssfilename,*ssindexes=args.ssf.split(':')
	chridx,posidx,eaidx,Bidx=[int(x)-1 for x in ssindexes]
	ss={}
	ssfile=safe_open(ssfilename)
	for line in ssfile:
		ssl=line.rstrip('\r\n').split('\t')
		ss[ssl[chridx]+"_"+ssl[posidx]]=(ssl[eaidx],float(ssl[Bidx]))
	ssfile.close()
	print("Summary statistics loaded: "+str(len(ss))+" associated snps ["+timethis()+"]",file=stderr,flush=True)
	print("Minimum snps in partial PS: "+str(args.minsnps),file=stderr,flush=True)

	#sum computation
	diffaasites=0
	prevchr=""
	t=[]
	if args.old:
		mypstable=PStableold(indlist,ancs,refindidx)
	else:
		mypstable=PStable(indlist,ancs,len(ss.keys()),refindidx)
	for line in vcffile:
		starttime=time.time()
		vcfcoord=re.match(r'([^\t]+)\t([0-9]+)', line)
		#if prevchr!=vcfcoord.group(1) and prevchr != "":
			#print('chromosome '+prevchr+' done',file=stderr,flush=True)
		prevchr=vcfcoord.group(1)
		try:
			myss=ss[vcfcoord.group(1)+"_"+vcfcoord.group(2)]
		except KeyError:
			continue
		status=mypstable.addsnp(myss[0],myss[1],line,args.threads)
		if status==1:
			diffaasites+=1
		t.append(time.time()-starttime)
		if mypstable.m==50:
			print("expected time until finished:"+str(timedelta(seconds=np.mean(t)*len(ss))),file=stderr,flush=True)
	vcffile.close()
	missingsites=len(ss)-mypstable.m-diffaasites

	print('Associated sites absent in vcf file:'+str(missingsites),file=stderr)
	print('Associated sites with effect allele absent in vcf file:'+str(diffaasites),file=stderr)
	print("Sum done ["+timethis()+"]",file=stderr)

	#standardization and print output
	#print(mypstable.count,file=stderr)
	mypstable.printout(args.print_all,args.minsnps,args.p)
	print("Standardization and output done ["+timethis()+"]\nFinished",file=stderr)

if __name__ == '__main__':
	main()
